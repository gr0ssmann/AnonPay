# AnonPay

AnonPay helps to enhance privacy in online experiments. **For the first time in experimental history, AnonPay made it possible to safely collect payment details within the experiment.**

## Overview

- Privacy enhancement through *data erasure*
	1. CPY: Separate payment details from behavioral dataset
	1. TDY: Redact the behavioral dataset

CPY and TDY must be used in combination.

## oTree usage

Set `player.participant.vars["payment"]` to the subject's payment amount. Then, put the `AnonPay` app at the end of your session config.
